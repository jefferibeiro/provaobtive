package br.com.provaObjetive.implementacao;

import javax.swing.JOptionPane;

import br.com.provaObjetive.interfaces.Controle;

public class Final implements Controle {
	
	public void respostaPositiva() {
		JOptionPane.showMessageDialog(null, "Acertei de novo!");
	}
	
	
	public void respostaNegativa(Controle controle) {
		String prato = JOptionPane.showInputDialog("Qual prato você pensou?");
        String caracteristica = JOptionPane.showInputDialog(
        	String.format("%s é_______mas %s não.", prato, controle.nomePrato())
        );
        controle.adicionaNivel(prato, caracteristica);
	}

	
	public String nomePrato() {
		return "";
	}

	
	public void adicionaNivel(String respostaPositiva, String respostaNegativa) {
	}
	
	
	public void pergunta() {
	}
	
}
