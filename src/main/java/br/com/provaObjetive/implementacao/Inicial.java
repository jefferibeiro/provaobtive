package br.com.provaObjetive.implementacao;

import javax.swing.JOptionPane;

import br.com.provaObjetive.interfaces.Controle;

public class Inicial implements Controle {
	
	private String nomePrato;
	
	private Controle iniPositivo;
	private Controle iniNegativo;
	
	public Inicial(String nomePrato) {
		this.nomePrato = nomePrato;
		this.iniPositivo = new Final();
		this.iniNegativo = new Final();
	}
	
	public void defineOpcoesIniciais(String opcaoPositiva, String opcaoNegativa) {
		this.iniPositivo = new Inicial(opcaoPositiva);
		this.iniNegativo = new Inicial(opcaoNegativa);
	}

	public void pergunta() {
		String pergunta = String.format("O prato que você pensou é %s?", this.nomePrato);
		int resposta = JOptionPane.showConfirmDialog(null, pergunta, "Confirm", JOptionPane.YES_NO_OPTION);
		
		if ( Integer.valueOf(resposta).equals(JOptionPane.YES_OPTION) ) {
			this.iniPositivo.respostaPositiva();
		} else {
			this.iniNegativo.respostaNegativa(this);
		}
	}

	
	public void respostaPositiva() {
		this.pergunta();
	}

	
	public void respostaNegativa(Controle controle) {
		this.pergunta();
	}

	
	public String nomePrato() {
		return this.nomePrato;
	}

	
	public void adicionaNivel(String respostaPositiva, String respostaNegativa) {
		this.iniPositivo = new Inicial(respostaPositiva);
		this.iniNegativo = new Inicial(this.nomePrato);
		this.nomePrato = respostaNegativa;
	}
}
