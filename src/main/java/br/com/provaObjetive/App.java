package br.com.provaObjetive;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import br.com.provaObjetive.implementacao.Inicial;

public class App 
{
	
	public static void main(String[] args) {
		App app = new App();
		app.start();		
	}
	
	private void start() {
		final Inicial ini = new Inicial("massa");
		ini.defineOpcoesIniciais("Lazanha", "Bolo de Chocolate");
		
		JFrame jframe = new JFrame("Jogo Gourmet");
		JLabel jlabel = new JLabel("Pense em um prato que gosta.");
		JButton jbuttonOk = new JButton("OK");
		
        jframe.setSize(285, 130);
        jframe.setLocationRelativeTo(null);
		jframe.setVisible(true);
		jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jframe.setLayout(null);
		
		jlabel.setBounds(50, 10, 200, 30);
		jframe.add(jlabel);
		
		jbuttonOk.setBounds(80, 40, 100, 30);
		jframe.add(jbuttonOk);
		
		jbuttonOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ini.pergunta();
			}
		});
	}
}

